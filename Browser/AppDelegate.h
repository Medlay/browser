//
//  AppDelegate.h
//  Browser
//
//  Created by Василь Скрипій on 19.07.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "BrowserController.h"

@interface AppDelegate : NSObject <NSApplicationDelegate>{
    IBOutlet BrowserController       *_browserController;
}

@property (assign) IBOutlet NSWindow *window;

@end
