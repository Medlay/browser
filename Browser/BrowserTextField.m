//
//  BrowserTextField.m
//  Browser
//
//  Created by Василь Скрипій on 19.07.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BrowserTextField.h"

@implementation BrowserTextField

@synthesize progress = _progress;

-(void)drawRect:(NSRect)dirtyRect {
    NSRect progressRect = [self bounds];
    progressRect.size.width *= _progress;
    
    [[NSColor colorWithCalibratedRed:1.0 green:0.8 * (1 - _progress) blue:0.8 * (1 - _progress) alpha:0.6] set];
    NSRectFillUsingOperation(progressRect, NSCompositeSourceAtop);
    
    [[NSColor colorWithCalibratedRed:1.0 green:1.0 blue:1.0 alpha:1.0] set];
    NSRectFillUsingOperation(CGRectMake(progressRect.size.width, 0, 
                                        [self bounds].size.width - progressRect.size.width, 
                                        [self bounds].size.height), NSCompositeSourceOver);
    
    [super drawRect:dirtyRect];
}


@end
