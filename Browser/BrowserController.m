//
//  BrowserController.m
//  Browser
//
//  Created by Василь Скрипій on 19.07.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BrowserController.h"

@implementation BrowserController

#define kDefaultSite @"google.com"
#define kTimeForClosing 180.0


- (void)dealloc{
    [_urlsHistory release];
    [super dealloc];
}


- (void)start{
    [_url setDrawsBackground:NO];
    [_webView setFrameLoadDelegate:self];
    [_webView setUIDelegate:self];
    [_webView setTextSizeMultiplier:1.1];
    _url.backgroundColor = [NSColor clearColor];
    _shouldSwallowThisReturn = YES;
    _reload = NO;
    _controlPressed = NO;
    _urlsHistory = [[NSMutableArray alloc] init];
    CALayer *viewLayer = [CALayer layer];
    [viewLayer setBackgroundColor:CGColorCreateGenericRGB(0.0, 0.0, 0.0, 1.0)];
    [_endView setWantsLayer:YES]; 
    [_endView setLayer:viewLayer];

    [_rememberQuit setTarget:self];
    [_quit setTarget:self];
    [_rememberQuit setSel:@selector(rememberAndQuit:)];
    [_quit setSel:@selector(quit:)];
    
    
    NSFont *font = [NSFont fontWithName:@"Arial" size:40.0];
    [_endText setFont:font];
    [_endText sizeToFit];
    
    NSString *url = [[NSUserDefaults standardUserDefaults] valueForKey:@"browser-saved-current-address"];
    if (!url) {
        url = kDefaultSite;
    }
    
    if (!url || [url isEqualToString:@""]) {
        return;
    }
    if ([url rangeOfString:@"://"].location == NSNotFound) {
        url = [NSString stringWithFormat:@"http://%@", url];
    }
    [_urlsHistory addObject:url];
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:url]];
    [[_webView mainFrame] loadRequest:request];
    _reload = YES;
    [_reloadButton setImage:[NSImage imageNamed:@"stop.png"]];
    [_url setStringValue:url];
    
    _timer = [[NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerFire:) userInfo:nil repeats:YES] retain];
    _currentTime = 0;
    _currentAddress = 0;
    [_control setEnabled:NO forSegment:0];
    [_control setEnabled:NO forSegment:1];
    NSText* fieldEditor = [[_url window] fieldEditor:YES forObject:_url];
    [fieldEditor setSelectedRange:NSMakeRange([[fieldEditor string]
                                               length],0)];
    [fieldEditor setNeedsDisplay:YES];
    [[_url window] makeFirstResponder:nil];
}


- (IBAction)controlPressed:(id)sender{
    NSSegmentedControl *control = (NSSegmentedControl *)sender;
    int selectedSegment = [control selectedSegment];
    if (selectedSegment == 0) {
        _currentAddress--;
        if (_currentAddress == 0) {
            [_control setEnabled:NO forSegment:0];
        }
        [_control setEnabled:YES forSegment:1];
    }else{
        _currentAddress++;
        if (_currentAddress == [_urlsHistory count] - 1) {
            [_control setEnabled:NO forSegment:1];
        }
        [_control setEnabled:YES forSegment:0];
    }
    _controlPressed = YES;
    NSString *url = [_urlsHistory objectAtIndex:_currentAddress];
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:url]];
    [[_webView mainFrame] loadRequest:request];
    _reload = YES;
    [_reloadButton setImage:[NSImage imageNamed:@"stop.png"]];
    [_url setStringValue:url];
}



- (IBAction)urlPressed:(id)sender{
    if (_shouldSwallowThisReturn) {
        _shouldSwallowThisReturn = NO;
        return;
    }
    NSTextField *textField = (NSTextField *)sender;
    NSString *url = [textField stringValue];
    if (!url || [url isEqualToString:@""]) {
        return;
    }
    if ([url rangeOfString:@"://"].location == NSNotFound) {
        url = [NSString stringWithFormat:@"http://%@", url];
    }
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:url]];
    [[_webView mainFrame] loadRequest:request];
    [_url setStringValue:url];
    _reload = YES;
    [_reloadButton setImage:[NSImage imageNamed:@"stop.png"]];
    while ([_urlsHistory count] != _currentAddress + 1) {
        [_urlsHistory removeLastObject];
    }
    [_control setEnabled:NO forSegment:1];
    [_urlsHistory addObject:url];
    _currentAddress++;
    _shouldSwallowThisReturn = NO;
    if ([_urlsHistory count] > 1) {
        [_control setEnabled:YES forSegment:0];
    }
}


- (IBAction)reload:(id)sender{
    if (_reload) {
        _reload = NO;
        [_webView stopLoading:nil];
        [_statusText setStringValue:@"Failed to open page."];
        [_reloadButton setImage:[NSImage imageNamed:@"reload.png"]];
    }
    else{
        _reload = YES;
        [_webView reload:nil];
        [_reloadButton setImage:[NSImage imageNamed:@"stop.png"]];
    }
}


- (void)timerFire:(NSTimer *)sender{    
    _url.progress = _currentTime / kTimeForClosing;
    [_url setNeedsDisplay:YES];
    int minutes = (kTimeForClosing - _currentTime) / 60;
	int seconds = (kTimeForClosing - _currentTime) - minutes * 60;
    NSString *secondsString = nil;
	NSString *minutesString = nil;
	if (seconds < 10)
		secondsString = [NSString stringWithFormat:@"0%d", seconds];
	else
		secondsString = [NSString stringWithFormat:@"%d", seconds];
	if (minutes < 10)
		minutesString = [NSString stringWithFormat:@"0%d", minutes];
	else
		minutesString = [NSString stringWithFormat:@"%d", minutes];
    NSString *time = [NSString stringWithFormat:@"%@:%@", minutesString, secondsString];
    [_time setStringValue:time];
    
    if (_currentTime == kTimeForClosing) {
        [_timer invalidate];
        [_timer release];
        _timer = nil;
        for (NSView *view in [[_webView superview] subviews]) {
            if ([view respondsToSelector:@selector(setEnabled:)]) {
                [(id)view setEnabled:NO];
            }
        }
        _endView.frame = _webView.frame;
        [[_webView superview] addSubview:_endView];
        
        _rememberQuit.frame = CGRectMake(_endText.frame.origin.x - 20, _endText.frame.origin.y - 70, _rememberQuit.frame.size.width, _rememberQuit.frame.size.height);
        _quit.frame = CGRectMake(_endText.frame.origin.x + 220, _endText.frame.origin.y - 70, _quit.frame.size.width, _quit.frame.size.height);
        [[_webView superview].window setMaxSize:[_webView superview].window.frame.size];
        [[_webView superview].window setMinSize:[_webView superview].window.frame.size];
        
        return;
    }
    _currentTime++;
}


- (IBAction)rememberAndQuit:(id)sender{
    [[NSUserDefaults standardUserDefaults] setValue:[_urlsHistory objectAtIndex:_currentAddress]
                                             forKey:@"browser-saved-current-address"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[NSApplication sharedApplication] terminate:self];
}


- (IBAction)quit:(id)sender{
    [[NSApplication sharedApplication] terminate:self];
}


#pragma mark WebView delegate methods

- (void)webView:(WebView *)sender didStartProvisionalLoadForFrame:(WebFrame *)frame{
    if (frame != sender.mainFrame) {
        return;
    }
    if (_shouldSwallowThisReturn) {
        _shouldSwallowThisReturn = NO;
        return;
    }
    [_url setStringValue:[sender mainFrameURL]];
    [_statusText setStringValue:[NSString stringWithFormat:@"Contacting \"%@\"", [sender mainFrameURL]]];
    if (!_controlPressed && !_reload) {
        while ([_urlsHistory count] != _currentAddress + 1) {
            [_urlsHistory removeLastObject];
        }
        [_urlsHistory addObject:[sender mainFrameURL]];
        _currentAddress++;
        [_control setEnabled:NO forSegment:1];
        [_control setEnabled:YES forSegment:0];
    }else {
        _controlPressed = NO;
    }
    if (!_reload) {
        _reload = YES;
        [_reloadButton setImage:[NSImage imageNamed:@"stop.png"]];
    }
    _oldStatusMessage = @"";
}


- (void)webView:(WebView *)sender didFinishLoadForFrame:(WebFrame *)frame{
    if (frame != sender.mainFrame) {
        return;
    }
    _reload = NO;
    [_reloadButton setImage:[NSImage imageNamed:@"reload.png"]];
    [_statusText setStringValue:@""];
    _oldStatusMessage = @"";
}


- (WebView *)webView:(WebView *)sender createWebViewWithRequest:(NSURLRequest *)request{
    return _webView;
}


- (void)webView:(WebView *)sender didCommitLoadForFrame:(WebFrame *)frame{
    if (frame != sender.mainFrame) {
        return;
    }
    [_statusText setStringValue:[NSString stringWithFormat:@"Loading \"%@\"", [sender mainFrameURL]]];
}


- (void)webView:(WebView *)sender didFailProvisionalLoadWithError:(NSError *)error forFrame:(WebFrame *)frame{
    if (frame != sender.mainFrame) {
        return;
    }
    [_statusText setStringValue:@"Failed to open page."];
    _oldStatusMessage = @"Failed to open page.";
    _controlPressed = NO;
}


- (void)webView:(WebView *)sender didFailLoadWithError:(NSError *)error forFrame:(WebFrame *)frame{
    if (frame != sender.mainFrame) {
        return;
    }
    [_statusText setStringValue:@"Failed to open page."];
    _oldStatusMessage = @"Failed to open page.";
    _controlPressed = NO;
}


- (void)webView:(WebView *)sender mouseDidMoveOverElement:(NSDictionary *)elementInformation modifierFlags:(NSUInteger)modifierFlags{
    if ([elementInformation valueForKey:@"WebElementLinkURL"]) {
        [_statusText setStringValue:[NSString stringWithFormat:@"Go to \"%@\"", [elementInformation valueForKey:@"WebElementLinkURL"]]];
    }else{
        if ([_oldStatusMessage isEqualToString:@"Failed to open page."]) {
            [_statusText setStringValue:_oldStatusMessage];
        }else{
            [_statusText setStringValue:@""];
        }
    }
}




#pragma mark Keyboard

- (IBAction)zoomIn:(id)sender{
    if ([_webView canMakeTextLarger]){
        [_webView makeTextLarger:nil];
    }
}


- (IBAction)zoomOut:(id)sender{
    if ([_webView canMakeTextSmaller]){
        [_webView makeTextSmaller:nil];
    }
}


@end
