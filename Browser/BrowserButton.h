//
//  BrowserButton.h
//  Browser
//
//  Created by Василь Скрипій on 20.07.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BrowserButton : NSView{
    SEL         _action;
    id          _target;
}

- (void)setSel:(SEL)sel;
- (void)setTarget:(id)target;

@end
