//
//  BrowserController.h
//  Browser
//
//  Created by Василь Скрипій on 19.07.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <WebKit/WebKit.h>
#import <WebKit/WebFrameLoadDelegate.h>
#import "BrowserTextField.h"
#import "BrowserButton.h"

@interface BrowserController : NSObject{
@private
    IBOutlet BrowserTextField       *_url;
    IBOutlet NSSegmentedControl     *_control;
    IBOutlet WebView                *_webView;
    IBOutlet NSTextField            *_time;
    IBOutlet NSView                 *_endView;
    IBOutlet NSTextField            *_endText;
    IBOutlet NSTextField            *_statusText;
    IBOutlet BrowserButton          *_rememberQuit;
    IBOutlet BrowserButton          *_quit;
    IBOutlet NSButton               *_reloadButton;
    IBOutlet NSTextField            *_stateText;
    
    NSTimer                         *_timer;
    float                            _currentTime;
    NSMutableArray                  *_urlsHistory;
    int                             _currentAddress;
    BOOL                            _shouldSwallowThisReturn;
    BOOL                            _reload;
    BOOL                            _controlPressed;
    NSString                        *_oldStatusMessage;
}

- (void)start;
- (IBAction)controlPressed:(id)sender;
- (IBAction)urlPressed:(id)sender;
- (IBAction)reload:(id)sender;
- (IBAction)rememberAndQuit:(id)sender;
- (IBAction)quit:(id)sender;
- (IBAction)zoomIn:(id)sender;
- (IBAction)zoomOut:(id)sender;

@end
