//
//  BrowserTextField.h
//  Browser
//
//  Created by Василь Скрипій on 19.07.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BrowserTextField : NSTextField{
    float               _progress;
}

@property (nonatomic) float                progress;

@end
