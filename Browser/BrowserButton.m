//
//  BrowserButton.m
//  Browser
//
//  Created by Василь Скрипій on 20.07.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BrowserButton.h"

@implementation BrowserButton

- (void) drawRect:(NSRect)dirtyRect
{
    self.layer.cornerRadius = 16;
    
    NSColor *color = [NSColor colorWithDeviceRed:255 / 153 green:255 / 153 blue:255 / 153 alpha:0.4];
    
    const NSInteger numberOfComponents = [color numberOfComponents];
    CGFloat components[numberOfComponents];
    CGColorSpaceRef colorSpace = [[color colorSpace] CGColorSpace];
    [color getComponents:(CGFloat *)&components];
    self.layer.backgroundColor = (CGColorRef)[(id)CGColorCreate(colorSpace, components) autorelease];
    
    for (NSTextField *field in [self subviews]) {
        NSFont *font = [NSFont fontWithName:@"Arial" size:20.0];
        [field setFont:font];
        [field sizeToFit];
    }
    
    [super drawRect:dirtyRect];
}


- (void)mouseDown:(NSEvent *)event
{
    [_target performSelector:_action withObject:self];
}


- (void)setSel:(SEL)sel{
    _action = sel;
}


- (void)setTarget:(id)target{
    _target = target;
}

@end
